package com.example.auth.ticket;

import com.example.auth.app.ulctools.Commands;
import com.example.auth.app.ulctools.Utilities;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Date.*;
import java.util.concurrent.TimeUnit;

/**
 * TODO: Complete the implementation of this class. Most of the code are already implemented. You
 * will need to change the keys, design and implement functions to issue and validate tickets.
 */
public class Ticket {

    private static byte[] authenticationKey = "BREAKMEIFYOUCAN!".getBytes();// 16-byte key
    private static byte[] masterKey = "MASTERSECRETKEY!".getBytes();
    private static byte applicationCode = (byte) 0xb4;

    /** TODO: Change these according to your design. Diversify the keys. */

    private static byte[] hmacKey = "0123456789ABCDEF".getBytes(); // min 16-byte key

    public static byte[] data = new byte[192];

    private static TicketMac macAlgorithm; // For computing HMAC over ticket data, as needed
    private static Utilities utils;
    private static Commands ul;

    private int remainingUses = 0;
    private int expiryTime = 0;

    private byte appByte = 0;
    private byte version = 0;
    private byte[] UID;

    private static String infoToShow; // Use this to show messages

    /** Create a new ticket */
    public Ticket() throws GeneralSecurityException {
        // Set HMAC key for the ticket
        macAlgorithm = new TicketMac();
        macAlgorithm.setKey(hmacKey);

        ul = new Commands();
        utils = new Utilities(ul);
    }

    /** After validation, get ticket status: was it valid or not? */
    public boolean isValid() {
        if (version == 1 && appByte == applicationCode){
            return true;
        }else{
            return false;
        }
    }

    private boolean readControlInfo(){
        return readControlInfo(false);
    }

    private boolean readControlInfo(boolean noFailOnControlInfo){
        boolean res;

        byte[] uidRead = new byte[12];
        //Read UID
        res = utils.readPages(0, 3, uidRead, 0);
        if(!res){
            Utilities.log("readControlInfo(): failed to read UID", true);
            return false;
        }
        UID = new byte[9];
        System.arraycopy(uidRead, 0, UID, 0, UID.length);

       byte[] controlInfo = new byte[4];
       res = utils.readPages(4, 1, controlInfo, 0);

       if (!res) {
           Utilities.log("readControlInfo(): failed to read control bytes", true);
           if(noFailOnControlInfo) {
               version = 1;
               appByte = 0;
           }else {
               return false;
           }
       }else {
           version = controlInfo[1];
           appByte = controlInfo[0];
       }
       return true;
    }


    private byte[] authKeyGen() throws NoSuchAlgorithmException{
        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        byte[] combinedSecret = new byte[UID.length + masterKey.length];
        System.arraycopy(UID, 0, combinedSecret, 0, UID.length);
        System.arraycopy(masterKey, 0, combinedSecret, UID.length, masterKey.length);
        byte[] key = md.digest(combinedSecret);
        return key;
    }

    private boolean authenticate(){
        boolean res;
        byte[] key;
        if(version == 1){ //MD5 hash of UID and master key
            System.out.println("Version 1");
            try {
                key = authKeyGen();
            }
            catch(NoSuchAlgorithmException e){
                e.printStackTrace();
                return false;
            }
        }else{
            System.out.println("Version 0");
            key = authenticationKey;
        }
        res = utils.authenticate(key);
        if (!res) {
            Utilities.log("Authentication failed in issue()", true);
            infoToShow = "Authentication failed";
        }

        return res;
    }

    /** After validation, get the number of remaining uses */
    public int getRemainingUses() {
        return remainingUses;
    }

    /** After validation, get the expiry time */
    public int getExpiryTime() {
        return expiryTime;
    }

    /** After validation/issuing, get information */
    public static String getInfoToShow() {
        String tmp = infoToShow;
        infoToShow = "";
        return tmp;
    }

    /**
     * Issue new tickets
     *
     */
    public boolean issue(int daysValid, int uses) throws GeneralSecurityException {
        boolean res = readControlInfo(true);
        if (!res) {
            return false;
        }
        res = authenticate();
        if (!res) {
            return false;
        }

        // Writing new auth key
        byte[] key = authKeyGen();
        res = utils.writePages(key, 0, 0x2C, 4);
        if(!res){
            Utilities.log("Issue: failed to write auth key", true);
            return false;
        }

        // Setting AUTH0 & AUTH1
        // Protection is starting from page 5, r&w operations are restricted
        byte[] authBytes = {0x5, 0, 0, 0, 0, 0, 0, 0};
        res = utils.writePages(authBytes, 0, 0x2A, 2);
        if(!res){
            Utilities.log("Issue: failed to write AUTH0 and AUTH1", true);
            return false;
        }

        // Writing version & app info
        byte[] controlInfo = {applicationCode, 1, 0, 0};
        res = utils.writePages(controlInfo, 0, 0x4, 1);
        if(!res){
            Utilities.log("Issue: failed to write control information", true);
            return false;
        }

        // Writing ticket info
        byte[] ticketInfo = {0, 0, 0, 0};
        ticketInfo[0] = (byte) uses;
        res = utils.writePages(ticketInfo, 0, 6, 1);

        // Set information to show for the user
        if (res) {
            infoToShow = "Wrote: uses = " + String.valueOf(ticketInfo[0]);
        } else {
            infoToShow = "Failed to write";
        }

        return true;
    }

    public boolean use() throws GeneralSecurityException, ParseException {
        // Read control information
        boolean res = readControlInfo();
        if (!res && !isValid()) {
            return false;
        }
        // Authenticate
        res = authenticate();
        if (!res) {
            Utilities.log("Authentication failed in issue()", true);
            infoToShow = "Authentication failed";
            return false;
        }

        // Read ticketinfo

        int timestamp = 0;

        byte[] ticketInfo = new byte[4];
        //read 6th page to get timestamp and usage info

        res = utils.readPages(6,1,ticketInfo,0);

        //timestamp t = ticketInfo[1] + ticketInfo[2] + ticketInfo[3]

        int uses = ticketInfo[0];

        // Set information to show for the user
        if (res) {
            infoToShow = "Read: " + new String(ticketInfo);

        } else {
            infoToShow = "Failed to read";
        }

        //decrease number of uses if read timestamp value is zero
        //if timestamp is set to zero - ticket is issued but never used, write current time stamp into ticket and decrease number of rides
        if(timestamp != 0){

            //check if hours have passed already


            //check uses left on ticket


            //

            utils.writePages(6,1,uses,0);
            if(uses == 0){
                infoToShow = "Last usage of ticket";
            }
            else{
                infoToShow = "Ticket used";
            }
        }
        else{ //otherwise, issue a new timestamp
            String startDate = "2020/01/01 00:00";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date date = sdf.parse(startDate);
            long startTime = date.getTime(); //time from beginning of 2020 as start time

            long now = System.currentTimeMillis(); //first usage of ticket time

            long tMinutes = TimeUnit.MILLISECONDS.toMinutes(now - startTime); //minutes since beginning of year acts as timestamp

            infoToShow = Long.toString(tMinutes);
            /**
             *
             * figure out how to use three bytes to store an unsigned number
             * for all the number lower than 2^24 - 1 (which is the max), you can just zero out the highest byte (4th),
             * as it anyway should be == 0 in an unsigned number
             */
            //utils.writePages(6,1,timestamp,0)
        }




        return true;
    }
}